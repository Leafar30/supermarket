'use strict'
let React = require('react-native');
let {StyleSheet} = React;
const constants = {
    actionColor:'#3cb371'
}

module.exports = StyleSheet.create({
    navbar:{
        alignItems:'center',
        backgroundColor:'pink',
        borderBottomColor: '#eee',
        borderColor: 'transparent',
        borderWidth:1,
        justifyContent: 'center',
        height: 44,
        flexDirection: 'row',
    },
    navbarTitle:{
        color:'#444',
        fontSize:16,
        fontWeight:"500"
    },
    toolbar:{
        backgroundColor: '#fff',
        height: 22
    },
    li:{
        backgroundColor: 'blue',
        borderBottomColor:'grey',
        borderColor:'transparent',
        borderWidth:1,
        paddingLeft:16,
        paddingTop:14,
        paddingBottom:16,

    },
    container:{
      flex:1,
      backgroundColor:'orange'
    },
    listview:{
        flex:1
    },
    liContainer:{
        flex:2
    },
    liText:{
        color:'#333',
        fontSize:16,
    },
    center: {
        textAlign: 'center',
    },
    actionText: {
        color: 'purple',
        fontSize: 20,
        textAlign: 'center'
    },
    action: {
        backgroundColor:constants.actionColor,
        borderColor: 'transparent',
        borderWidth: 1,
        paddingLeft: 16,
        paddingTop: 14,
        paddingBottom: 16,
    }
});