/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  TouchableHighlight,
  Modal,
  TextInput
} from 'react-native';
const styles = require('./apps/style');

import ToolBar from './apps/Components/ToolBar/ToolBar';
import AddButton from './apps/Components/AddButton/AddButton';

import * as firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyDoq8UNyfQVbHwsgeCQ07x_xBDIk1SIQEI",
  authDomain: "itemlister-c13e9.firebaseapp.com",
  databaseURL: "https://itemlister-c13e9.firebaseio.com",
  projectId: "itemlister-c13e9",
  storageBucket: "itemlister-c13e9.appspot.com"
}

const firebaseapp = firebase.initializeApp(firebaseConfig);

export default class GraphicProj extends Component {
  constructor(){
    super();
    let ds = new ListView.DataSource({rowHasChanged:(r1,r2) => r1 !== r2});
    this.state={
        text:'',
        itemDataSource: ds,
        modalVisible: false
    }
    this.itemsRef = this.getRef().child('items');

    this.renderRow = this.renderRow.bind(this);
    this.pressRow = this.pressRow.bind(this);
  }

  setModalVisible(visible) {
    this.setState({modalVisible:visible});
  }


  getRef(){
    return firebaseapp.database().ref();
  }
  
  componentWillMount(){
    this.getItems(this.itemsRef);
  }

  componentDidMount(){
    this.getItems(this.itemsRef);
  }


  getItems(itemsRef){
    //let items = [{title:'Item One'},{title:'Item two'} ];
    itemsRef.on('value',(snap) =>{
      let items = [];
      snap.forEach((child) => {
        items.push({
            title: child.val().title,
            _key: child.key
        });
      });
      this.setState({
        itemDataSource: this.state.itemDataSource.cloneWithRows(items)
      });
    
    });

      
  }

  pressRow(item){
    this.itemsRef.child(item._key).remove();
  }

  renderRow(item){
    return(
      <TouchableHighlight onPress={() => {
        this.pressRow(item);
        }} >
      <View style={styles.li}>
        <Text style={styles.liText}>{item.title}</Text>
        </View>
      </TouchableHighlight>  
    )
         
  }

  addItem(){
      this.setModalVisible(true);
  }

  render() {
    return (
      <View style={styles.container}>
        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {}}
          >
         <View style={{marginTop: 22}}>
          <View>
            <ToolBar title="Add Item" />
            <TextInput 
              value={this.state.text}
              placeholder="Add Item"
              onChangeText = {(value) => this.setState({text:value})}
              />

            <TouchableHighlight onPress={() => {
              this.itemsRef.push({title: this.state.text})
              this.setModalVisible(!this.state.modalVisible)
            }}>
              <Text>Save Item</Text>
            </TouchableHighlight>

            <TouchableHighlight onPress={() => {
              this.setModalVisible(!this.state.modalVisible)
            }}>
              <Text>Cancel</Text>
            </TouchableHighlight>


          </View>
         </View>
        </Modal>

     <ToolBar title="ItemLister"/>
     <ListView
        dataSource={this.state.itemDataSource}
        renderRow={this.renderRow} />
        <AddButton onPress={this.addItem.bind(this)} title="Add Item"/>
      </View>
    );
  }
}






AppRegistry.registerComponent('GraphicProj', () => GraphicProj);
