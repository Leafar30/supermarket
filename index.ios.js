/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  TouchableHighlight
} from 'react-native';
const styles = './app/style';

import ToolBar from './apps/Components/ToolBar/ToolBar';

export default class GraphicProj extends Component {
  constructor(){
    super();
    let ds = new ListView.DataSource({rowHasChanged:(r1,r2) => r1 !== r2});
    this.state={
        itemDataSource: ds
    }
    this.renderRow = this.renderRow.bind(this);
    this.pressRow = this.pressRow.bind(this);
  }
  
  componentWillMount(){
    this.getItems();
  }

  componentDiMount(){
    this.getItems();
  }


  getItems(){
    let items = [{title:'Item One'},{title:'Item two'} ];

      this.setState({
        itemDataSource: this.state.itemDataSource.cloneWithRows(items)
      })
  }

  pressRow(item){
    console.log(item);
  }

  renderRow(item){
    return(
      <TouchableHighlight onPress={() => {
        this.pressRow(item);
        }} >
      <View style={styles.li}>
        <Text style={styles.liText}>{item.title}</Text>
        </View>
      </TouchableHighlight>  
    )
         
  }

  render() {
    return (
      <View style={styles.container}>
     <ToolBar title="ItemLister"/>
     <ListView
        dataSource={this.state.itemDataSource}
        renderRow={this.renderRow} />
      </View>
    );
  }
}






AppRegistry.registerComponent('GraphicProj', () => GraphicProj);
